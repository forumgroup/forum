-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema forum
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema forum
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `forum` DEFAULT CHARACTER SET utf8 ;
USE `forum` ;

-- -----------------------------------------------------
-- Table `forum`.`user_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`user_details` (
  `user_details_id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(200) NOT NULL,
  `full_name` VARCHAR(45) NULL DEFAULT NULL,
  `profile_pic_url` VARCHAR(2000) NULL DEFAULT NULL,
  PRIMARY KEY (`user_details_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `forum`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(15) NOT NULL,
  `password` VARCHAR(68) NOT NULL,
  `enabled` TINYINT NOT NULL DEFAULT '1',
  `blocked` TINYINT NOT NULL DEFAULT '0',
  `user_details_user_details_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username` (`username` ASC) VISIBLE,
  INDEX `fk_users_user_details1_idx` (`user_details_user_details_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_user_details1`
    FOREIGN KEY (`user_details_user_details_id`)
    REFERENCES `forum`.`user_details` (`user_details_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `forum`.`authorities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`authorities` (
  `username` VARCHAR(25) NOT NULL,
  `authority` VARCHAR(25) NOT NULL,
  INDEX `authorities_fk0` (`username` ASC) VISIBLE,
  CONSTRAINT `authorities_fk0`
    FOREIGN KEY (`username`)
    REFERENCES `forum`.`users` (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `forum`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`tags` (
  `tag_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`tag_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `forum`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`posts` (
  `post_id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(256) NOT NULL,
  `author_id` INT NOT NULL,
  `tag_id` INT NOT NULL,
  `content` MEDIUMTEXT NOT NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`post_id`),
  INDEX `posts_fk0` (`author_id` ASC) VISIBLE,
  INDEX `fk_posts_tags1_idx` (`tag_id` ASC) VISIBLE,
  CONSTRAINT `posts_fk0`
    FOREIGN KEY (`author_id`)
    REFERENCES `forum`.`users` (`user_id`),
  CONSTRAINT `fk_posts_tags1`
    FOREIGN KEY (`tag_id`)
    REFERENCES `forum`.`tags` (`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `forum`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`comments` (
  `comment_id` INT NOT NULL AUTO_INCREMENT,
  `author_id` INT NOT NULL,
  `post_id` INT NOT NULL,
  `comment` MEDIUMTEXT NOT NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`comment_id`),
  INDEX `comments_fk0` (`author_id` ASC) VISIBLE,
  INDEX `comments_fk1` (`post_id` ASC) VISIBLE,
  CONSTRAINT `comments_fk0`
    FOREIGN KEY (`author_id`)
    REFERENCES `forum`.`users` (`user_id`),
  CONSTRAINT `comments_fk1`
    FOREIGN KEY (`post_id`)
    REFERENCES `forum`.`posts` (`post_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `forum`.`posts_has_comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`posts_has_comments` (
  `posts_post_id` INT NOT NULL,
  `comments_comment_id` INT NOT NULL,
  PRIMARY KEY (`posts_post_id`, `comments_comment_id`),
  INDEX `fk_posts_has_comments_comments1_idx` (`comments_comment_id` ASC) VISIBLE,
  INDEX `fk_posts_has_comments_posts1_idx` (`posts_post_id` ASC) VISIBLE,
  CONSTRAINT `fk_posts_has_comments_comments1`
    FOREIGN KEY (`comments_comment_id`)
    REFERENCES `forum`.`comments` (`comment_id`),
  CONSTRAINT `fk_posts_has_comments_posts1`
    FOREIGN KEY (`posts_post_id`)
    REFERENCES `forum`.`posts` (`post_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
