package com.demo.repositories;

import com.demo.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Integer> {

    Post findByTitle(String title);

    List<Post> getAllByCategoryName(String categoryName);
}
