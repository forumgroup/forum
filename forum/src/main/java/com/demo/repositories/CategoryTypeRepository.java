package com.demo.repositories;

import com.demo.models.CategoryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryTypeRepository extends JpaRepository<CategoryType,Integer> {
    CategoryType getByName(String name);
}
