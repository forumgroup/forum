package com.demo.repositories;

import com.demo.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {

    Category getById(Integer id);

    Category getByName(String name);

    List<Category> getAllByTypeName(String type);
}
