package com.demo.controllers;

import com.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class UserController {

    private final UserService userService;



    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/profile")
    public ModelAndView showProfile(Principal principal) {
        ModelAndView mav = new ModelAndView("profile");
        mav.addObject("user", userService.getByUsername(principal.getName()));
        /*mav.addObject("updateUser",new UserDto());*/
        return mav;
    }

/*    @PostMapping("/profile/edit")
    public ModelAndView editUser(Principal principal, @Valid @ModelAttribute UserDto updatedUserInfo) {
        ModelAndView mav = new ModelAndView("redirect:/profile");
        updatedUserInfo.setUsername(principal.getName());
        userService.updateUser(updatedUserInfo);
        return mav;
    }*/


}