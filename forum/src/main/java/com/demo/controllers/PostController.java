package com.demo.controllers;


import com.demo.models.Post;
import com.demo.models.dtos.AddCommentDto;
import com.demo.models.dtos.GeneratePostDto;
import com.demo.services.contracts.CategoryService;
import com.demo.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;


@Controller
public class PostController {
    private final PostService postService;
    private final CategoryService categoryService;

    @Autowired
    public PostController(PostService postService, CategoryService categoryService) {
        this.postService = postService;
        this.categoryService = categoryService;
    }

    @GetMapping("/posts")
    public ModelAndView getSinglePost(@RequestParam(required = false) Integer id) {
        if (id != null) {
            ModelAndView mav = new ModelAndView("blog-single");
            mav.addObject("post", postService.getPostById(id));
            mav.addObject("comment", new AddCommentDto());
            mav.addObject("mainCategories", categoryService.getMainCategories());
            return mav;
        }
        ModelAndView mav = new ModelAndView("posts");
        mav.addObject("posts", postService.getAll());
        mav.addObject("mainCategories", categoryService.getMainCategories());
        return mav;

    }

    @GetMapping("/posts/{category}")
    public ModelAndView getAllPostByCategory(@PathVariable String category) {
        ModelAndView mav = new ModelAndView("posts");
        mav.addObject("posts", postService.getAllByCategory(category));
        mav.addObject("mainCategories", categoryService.getMainCategories());
        return mav;
    }


    @GetMapping("/posts/generate")
    public ModelAndView showCreatePost() {
        ModelAndView mav = new ModelAndView("createPost");
        mav.addObject("post", new GeneratePostDto());
        mav.addObject("tags", categoryService.getAll());
        mav.addObject("mainCategories", categoryService.getMainCategories());
        mav.addObject("categories", categoryService.getAll());
        return mav;
    }

    @PostMapping("/posts/generate")
    public ModelAndView generatePost(@ModelAttribute GeneratePostDto generatePostDto, Principal principal) {
        generatePostDto.setAuthor(principal.getName());
        Post post = postService.generatePost(generatePostDto);
        return new ModelAndView("redirect:/posts?id=" + post.getId());
    }


    @PostMapping("/posts/{id}/comment")
    public ModelAndView addComment(@PathVariable int id, @ModelAttribute AddCommentDto addCommentDto, Principal principal) {
        addCommentDto.setAuthor(principal.getName());
        addCommentDto.setPostId(id);
        postService.addComment(addCommentDto);

        return new ModelAndView("redirect:/posts/" + id);
    }


}
