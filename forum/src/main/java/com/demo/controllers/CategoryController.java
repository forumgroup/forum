package com.demo.controllers;

import com.demo.models.Category;
import com.demo.models.dtos.GenerateCategoryDto;
import com.demo.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories/generate")
    public ModelAndView showCreateCategory(@RequestParam(required = false) Integer id) {
        ModelAndView mav = new ModelAndView("createCategory");
        mav.addObject("category", new GenerateCategoryDto());
        mav.addObject("categories", categoryService.getAll());
        mav.addObject("mainCategories", categoryService.getMainCategories());
        mav.addObject("selectedCategoryId",id);
        return mav;
    }

    @PostMapping("/categories/generate")
    public ModelAndView generatePost(@ModelAttribute GenerateCategoryDto generateCategoryDto) {
        Category category = new Category();
        if (generateCategoryDto.getCategory().equals("0")) {
            category.setName(generateCategoryDto.getName());
            categoryService.generateMainCategory(category);
        } else {
            category.setName(generateCategoryDto.getName());
            categoryService.generateSubCategory(categoryService.getById(Integer.parseInt(generateCategoryDto.getCategory())), category);
        }
        return new ModelAndView("redirect:/posts/");
    }

}
