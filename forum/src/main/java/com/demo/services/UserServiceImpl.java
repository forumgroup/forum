package com.demo.services;

import com.demo.models.User;
import com.demo.models.UserDetails;
import com.demo.models.dtos.UserRegisterDto;
import com.demo.repositories.UserRepository;
import com.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    public static final String IMAGE_PATH_FOR_USER = "users/";
    private static final int ENABLED = 1;
    private static final int DISABLED = 0;
    private static final String DEFAULT_USER_IMAGE = "/images/users/default-avatar.png";
    private static final String USER_DONT_EXIST = "This user does not exist!";
    private static final String ABV_ABV_BG = "abv@abv.bg";
    private final UserRepository userRepository;
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void createUser(UserRegisterDto user) {

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);
    }

    @Override
    public boolean userExist(String username) {
        return userDetailsManager.userExists(username);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getByUserId(Integer userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public void addDefaultUserDetails(String username) {

        User user = getByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException(USER_DONT_EXIST);
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setEmail(ABV_ABV_BG);
        userDetails.setPictureURL(DEFAULT_USER_IMAGE);
        user.setUserDetails(userDetails);

        userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }


}
