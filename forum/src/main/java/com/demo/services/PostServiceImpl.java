package com.demo.services;

import com.demo.models.Comment;
import com.demo.models.Post;
import com.demo.models.dtos.AddCommentDto;
import com.demo.models.dtos.GeneratePostDto;
import com.demo.repositories.CategoryRepository;
import com.demo.repositories.PostRepository;
import com.demo.repositories.UserRepository;
import com.demo.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository, CategoryRepository categoryRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
    }

    public Post generatePost(GeneratePostDto generatePostDto) {
        Post post = new Post();
        post.setTitle(generatePostDto.getTitle());
        post.setPostContent(generatePostDto.getPostContent());
        post.setDate(new Date(System.currentTimeMillis()));
        post.setAuthor(userRepository.findByUsername(generatePostDto.getAuthor()));
        post.setCategory(categoryRepository.getById(Integer.parseInt(generatePostDto.getCategory())));
        postRepository.save(post);
        return findByTitle(post.getTitle());
    }

    public Post findByTitle(String title) {
        return postRepository.findByTitle(title);
    }

    public Post getPostById(int id) {
        return postRepository.getOne(id);
    }

    public List<Post> getAll() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> getAllByCategory(String categoryName) {
        return postRepository.getAllByCategoryName(categoryName);
    }

    @Override
    public void addComment(AddCommentDto addCommentDto) {
        Post post = getPostById(addCommentDto.getPostId());
        Comment comment = new Comment();
        comment.setAuthor(userRepository.findByUsername(addCommentDto.getAuthor()));
        comment.setComment(addCommentDto.getCommentContent());
        comment.setDate(new Date(System.currentTimeMillis()));
        comment.setPost(post);
        post.getComments().add(comment);
        postRepository.save(post);
    }
}
