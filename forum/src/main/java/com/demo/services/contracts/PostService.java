package com.demo.services.contracts;

import com.demo.models.Post;
import com.demo.models.dtos.AddCommentDto;
import com.demo.models.dtos.GeneratePostDto;

import java.util.List;

public interface PostService {

    Post generatePost(GeneratePostDto generatePostDto);

    Post findByTitle(String title);

    Post getPostById(int id);

    List<Post> getAll();

    List<Post> getAllByCategory(String category);

    void addComment(AddCommentDto addCommentDto);
}
