package com.demo.services.contracts;

import com.demo.models.User;
import com.demo.models.dtos.UserRegisterDto;

import java.util.List;

public interface UserService {

    void addDefaultUserDetails(String username);

    void createUser(UserRegisterDto user);

    boolean userExist(String username);

    User getByUsername(String username);

    User getByUserId(Integer userId);

    List<User> getAll();

}
