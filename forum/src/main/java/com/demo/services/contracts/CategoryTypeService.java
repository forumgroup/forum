package com.demo.services.contracts;

import com.demo.models.CategoryType;

public interface CategoryTypeService {
    CategoryType getByCategoryType(String categoryType);
}
