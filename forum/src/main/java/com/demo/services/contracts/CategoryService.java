package com.demo.services.contracts;

import com.demo.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();

    Category getById(Integer id);

    Category getByName(String name);

    Category generateMainCategory(Category category);

    Category generateSubCategory(Category mainCategory, Category subCategory);

    List<Category> getMainCategories();
}
