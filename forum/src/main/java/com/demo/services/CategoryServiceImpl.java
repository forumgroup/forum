package com.demo.services;

import com.demo.models.Category;
import com.demo.repositories.CategoryRepository;
import com.demo.repositories.CategoryTypeRepository;
import com.demo.services.contracts.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private static final String MAIN = "Main";
    private static final String SUB = "Sub";
    private final CategoryRepository categoryRepository;
    private final CategoryTypeRepository categoryTypeRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryTypeRepository categoryTypeRepository) {
        this.categoryRepository = categoryRepository;
        this.categoryTypeRepository = categoryTypeRepository;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getById(Integer id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.getByName(name);
    }

    @Override
    public Category generateMainCategory(Category category) {
        category.setType(categoryTypeRepository.getByName(MAIN));
        categoryRepository.save(category);
        return getByName(category.getName());
    }

    @Override
    public List<Category> getMainCategories() {
        return categoryRepository.getAllByTypeName("Main");
    }

    @Override
    public Category generateSubCategory(Category mainCategory, Category subCategory) {
        subCategory.setType(categoryTypeRepository.getByName(SUB));
        categoryRepository.save(subCategory);
        mainCategory.getSubCategories().add(subCategory);
        categoryRepository.save(mainCategory);
        return getByName(subCategory.getName());
    }
}
