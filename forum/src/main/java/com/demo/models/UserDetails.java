package com.demo.models;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "user_details")
public class UserDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_details_id")
    private Integer id;

    @Column(name = "email")
    private String email;

    @Column(name = "full_name")
    private String fullName;


    @Column(name = "profile_pic_url")
    private String pictureURL;
}
