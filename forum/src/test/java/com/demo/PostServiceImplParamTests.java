package com.demo;

import com.demo.models.Post;
import com.demo.services.contracts.PostService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(Parameterized.class)
public class PostServiceImplParamTests {

    private final Post post;
    private final String title;
    private final int id;
    @Mock
    PostService postService;

    public PostServiceImplParamTests(String title, int id, Post post) {
        this.id = id;
        this.title = title;
        this.post = post;
    }

    private static Post createPost(int id, String title) {
        Post post = new Post();
        post.setId(id);
        post.setTitle(title);
        return post;
    }

    @Parameters
    public static List<Object[]> init() {
        return Arrays.asList(new Object[][]{
                {"title1", 1, createPost(1, "title1")},
                {"title2", 2, createPost(2, "title2")}
        });
    }

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void shouldReturnExpectedTitlePost() {
        Mockito.when(postService.findByTitle(title)).thenReturn(post);

        Post result = postService.findByTitle(title);

        Assert.assertEquals(result, post);
        Assert.assertEquals(result.getTitle(), post.getTitle());
    }

    @Test
    public void shouldReturnExpectedIdPost() {
        Mockito.when(postService.getPostById(id)).thenReturn(post);

        Post result = postService.getPostById(id);

        Assert.assertEquals(result, post);
        Assert.assertEquals(result.getId(), post.getId());
    }
}
