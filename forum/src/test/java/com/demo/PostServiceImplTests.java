package com.demo;

import com.demo.models.Post;
import com.demo.services.contracts.PostService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTests {
    private static final String POST_TITLE = "PostTitle";

    @Mock
    PostService postService;

    private Post createPost() {
        Post post = new Post();
        post.setId(1);
        post.setTitle(POST_TITLE);
        return post;
    }


    @Test
    public void getAllPosts() {
        Mockito.when(postService.getAll()).thenReturn(Arrays.asList(createPost(), createPost()));

        Assert.assertEquals(2, postService.getAll().size());
    }
}
